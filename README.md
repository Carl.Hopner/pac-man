<img align="right" width=200 src="./src/main/resources/images/logo.png">

# Pac Man

Jeg har valgt å programmere det klassiske arkadespillet pac-man som opprinnelig ble laget av Toru Iwatani i selskapet Namco i 1980. I spillet så styrer du Pac-Man (gul) som skal spise alle mynter som finnes på brettet uten å selv bli spist av spøkelsene som jager han. Du kan styre pac-man med piltastene og sette spillet på pause med esc. I filen PacManModel.java er det en boolean `smartGhosts` man kan sette til true om man ønsker smartere spøkelser. Dette er en kode som ikke funker, men all kode for hvordan jeg hadde tenkt at det skulle funke finnes i programmet. 

[Trykk her for video](pacman-guide.mov)

## Oversikt over arkitektur

Koden er bassert på hvordan vi kodet Tetris i semesteroppgave 1 og den grunnleggende strukturen er så og si helt lik.

Dette leder oss til at vi deler inn koden vår i tre sentrale pakker: 
 - *inf101v22.pacman.model*
 - *inf101v22.pacman.view*
 - *inf101v22.pacman.controller*

I tillegg vil vi benytte oss av en datastruktur for *Grid* som er mer generell, og som skal være i pakken
 - *inf101.v22.grid*

Det hele kjøres fra `PacManMain` i pakken *inf101v22.pacman*, som inneholder main-metoden.

### Modell

For å representere et spill med Pac-Man, er det to hoved-elementer vi må holde styr på:
 - et *brett* med *fliser*, og
 - *spillere* på brettet i form av pac-man og spøkelsene.

I tråd med prinsippene for objekt-orientert programmering identifiserer vi følgende klasser som egner seg for pakken *inf101v22.pacman.model*:
 - `PacManModel` en klasse som representerer tilstanden til et komplett spill med Pac-Man. Denne klassen vil ha feltvariabler som representerer brettet med fliser, pac-man selv, spøkelsene, diverse tall som for eksempel score, samt informasjon om spillet er game over.
 - `PacManBoard` en klasse som representerer et brett med fliser. Dette er i bunn og grunn en variant av Grid som holder på fliser, men har en bestemt størrelse og bestemte koordinater hvor det blir laget vegger.
 - `Tile` en klasse som representerer en flis på brettet.
 - `PacManPlayer` er en klasse som representerer pac-man som en flis, hvor han befinner seg og hvilken status han har (retning og om han er død).
 - `Ghosts` er en klasse som representerer spøkelsene i spillet. den har en del feltvariabler som holder på en flis, posisjon, spawn, hvor spøkelset er på vei, og om spøkelset prøver å fange eller rømme fra pac-man. Ideen er at spøkelsene skal bruke en labyrintløsnings-algoritme til å finne korteste vei til målet og så følge denne veien. Konstruktøren til klassen er private, men i klassen finnes en liste med de originale spøkelsene fra pac-man spillet.

 ### Visning

 For å vise modellen grafisk, lar vi det det være en klasse `PacManView` i pakken *inf101v22.pacman.view* som har som ansvar å tegne PacMan-modellen. For å tegne Pac-Man, er planen at PacManView
  - først tegner brettet (labyrinten), og
  - deretter tegner pac-man og spøkelsene.

PacManView må å ha tilgang til modellen for å kunne tegne den, men vi ønsker at vi ikke skal uforvarende kunne *endre* modellen når vi gjør ting i PacManView. For å innkapsle modellen vår, lar vi `PacManViewable` være et grensesnitt i pakken *inf101v22.pacman.view* som beskriver hvilke metoder PacManView behøver for å tegne et PacMan-brett. Så lar vi modellen PacManModel implementere dette grensesnittet.

### Kontroll

I pakken *inf101v22.pacman.controller* lar vi det være en klasse `PacManController` som har som sitt ansvarsområde å endre modellen basert på input fra brukeren, samt styre ting som skjer av seg selv.

På samme måte som for visningen, så er kontrolleren avhengig av tilgang til modellen. Samtidig vil vi innkapsle modellen så mye som mulig. Vi lar det derfor være et grensesnitt `PacManControllable` i pakken *inf101v22.pacman.controller* som beskriver hvilke metoder kontrolleren trenger tilgang til, og så lar vi modellen vår PacManModel implementere dette grensesnittet.

### Kilder
README.md er en endret utgave av README.md i semesteroppgave 1: Tetris skrevet av Torstein Strømme.
- Wikipedia. (2022, 29. april). "Maze-solving algorithm", https://en.wikipedia.org/wiki/Maze-solving_algorithm
- Strømme, Torstein. (2022, 29. april) `GraphicHelperMethods.java`

