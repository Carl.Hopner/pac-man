package inf101v22.grid;

import java.util.Objects;

/**
 * This class represents what item is contained on a specific 
 * cell in a grid.
 * 
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class CoordinateItem <E> {
    
    public final Coordinate coordinate;
    public final E item;

    public CoordinateItem(Coordinate cords, E item) {
        this.coordinate = cords;
        this.item = item;
    }


    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof CoordinateItem)) {
            return false;
        }
        CoordinateItem coordinateItem = (CoordinateItem) o;
        return Objects.equals(coordinate, coordinateItem.coordinate) && Objects.equals(item, coordinateItem.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate, item);
    }


    /**
     * Turns coordinate item into readable string.
     */
    public String toString() {
        return String.format("{ coordinate='%1$s', item='%2$s' }", coordinate.toString(), item);
    }
}
