package inf101v22.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A grid contains a set of CoordinateItems.
 * 
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class Grid<E> implements IGrid<E> {

    public int rows;
    public int cols;
    public List<List<E>> cells;

    public Grid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        cells = new ArrayList<>();
        for (int r=0; r<=rows; r++) {
            ArrayList<E> newList = new ArrayList<>();
            for (int c=0; c<=cols; c++) {
                newList.add(null);
            }
            cells.add(newList);
        }
    }
    
    public Grid(int rows, int cols, E value) {
        this.rows = rows;
        this.cols = cols;
        cells = new ArrayList<>();
        for (int r=0; r<=rows; r++) {
            ArrayList<E> newList = new ArrayList<>();
            for (int c=0; c<=cols; c++) {
                newList.add(value);
            }
            cells.add(newList);
        }
    }

    @Override
    public Iterator<CoordinateItem<E>> iterator() {
        ArrayList<CoordinateItem<E>> list = new ArrayList<CoordinateItem<E>>();
        for (int r=0; r<rows; r++) {
            for (int c=0; c<cols; c++) {
                Coordinate cords = new Coordinate(r,c);
                CoordinateItem<E> CellWithState = new CoordinateItem<E>(cords, get(cords));
                list.add(CellWithState);
            }
        }
        return list.iterator();
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public int getCols() {
        return cols;
    }

    @Override
    public void set(Coordinate coordinate, E value) {
        if (!coordinateIsOnGrid(coordinate)) {
            throw new IndexOutOfBoundsException("This coordinate is out of bounds");
        }
        cells.get(coordinate.row).set(coordinate.col, value);
        
    }

    @Override
    public E get(Coordinate coordinate) {
        if (!coordinateIsOnGrid(coordinate)) {
            throw new IndexOutOfBoundsException("This coordinate is out of bounds");
        }
        return cells.get(coordinate.row).get(coordinate.col);
    }

    @Override
    public boolean coordinateIsOnGrid(Coordinate coordinate) {
        if (coordinate.row < 0 || coordinate.col<0) {
            return false;
        }
        else if (coordinate.row > this.rows-1 || coordinate.col > this.cols-1) {
            return false;
        }
        else {
            return true;
        }
    }

    
}
