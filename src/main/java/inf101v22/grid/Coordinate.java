package inf101v22.grid;

import java.util.Objects;

/**
 * This class represents the coordinates of a cell on a grid defined by
 * which row and which column the cell is located.
 * 
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class Coordinate {
    
    public final int row;
    public final int col;

    public Coordinate(int row, int col){
        this.row = row;
        this.col = col;
    }


    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Coordinate)) {
            return false;
        }
        Coordinate coordinate = (Coordinate) o;
        return row == coordinate.row && col == coordinate.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
    

    /**
     * Turns coordinate into readable string.
     */
    public String toString() {
        return String.format("{ row='%1$s', col='%2$s' }", row, col);
    }

    /**
	 * Returns the Manhattan distance between 2 locations.
	 * That is the shortest distance distance between two points
	 * if you can only go East,West,North and South (not diagonally)
     * 
     * Taken from location.java in the Blob Wars task.
	 *
	 * @param loc
	 * @return
	 */
	public int gridDistanceTo(Coordinate loc) {
		return Math.abs(this.row - loc.row) + Math.abs(this.col - loc.col);
	}



}
