package inf101v22.pacman;

import javax.swing.JComponent;
import javax.swing.JFrame;

import inf101v22.pacman.controller.PacManController;
import inf101v22.pacman.model.PacManModel;
import inf101v22.pacman.view.*;

public class PacManMain {
    public static final String WINDOW_TITLE = "INF101 Pac-Man";

    public static void main(String[] args) {
        PacManModel model = new PacManModel();
        PacManView view = new PacManView(model);
        PacManController controller = new PacManController(model, view);
          
        // The JFrame is the "root" application window.
        // We here set som properties of the main window, 
        // and tell it to display our PacManView
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Here we set which component to view in our window
        frame.setContentPane(view);

        // Call these methods to actually display the window
        frame.pack();
        frame.setVisible(true);
    }
    
}
