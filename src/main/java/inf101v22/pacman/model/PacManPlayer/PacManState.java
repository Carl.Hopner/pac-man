package inf101v22.pacman.model.PacManPlayer;

public enum PacManState {

    UP,
    DOWN,
    LEFT,
    RIGHT,
    DEAD;
    
}
