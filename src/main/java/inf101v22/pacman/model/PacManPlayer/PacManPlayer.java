package inf101v22.pacman.model.PacManPlayer;

import java.awt.Color;

import javax.swing.ImageIcon;

import inf101v22.grid.Coordinate;
import inf101v22.pacman.model.Tile;

public class PacManPlayer {

    Coordinate position;
    Tile tile = new Tile(Color.YELLOW, 'p');
    PacManState state;


    public PacManPlayer(Coordinate position, PacManState state) {
        this.position = position;
        this.state = state;
    }

    public Tile getTile() {
        return this.tile;
    }

    public Coordinate getPosition() {
        return this.position;
    }

    public PacManState getState() {
        return this.state;
    }

    public void setPosition(Coordinate position) {
        this.position = position;
    }

    public void setState(PacManState state) {
        this.state = state;
    }
    
}
