package inf101v22.pacman.model.Ghosts;

import java.awt.Color;

import inf101v22.grid.Coordinate;
import inf101v22.pacman.model.PacManBoard;
import inf101v22.pacman.model.PacManModel;
import inf101v22.pacman.model.Tile;

public class Ghost {

    String name;
    GhostState state;
    Coordinate position;
    Tile tile;
    Coordinate target;
    Coordinate scatterTarget;
    Coordinate spawn;

    private Ghost(String name, Coordinate position, Coordinate target, Coordinate spawn, Coordinate scatterTarget, Tile tile) {
        this.name = name;
        this.position = position;
        this.target = target;
        this.scatterTarget = scatterTarget;
        this.spawn = spawn;
        this.tile = tile;
        this.state = GhostState.CHASE;
    }

    public Coordinate getPosition() {
        return this.position;
    }

    public Coordinate getTarget() {
        if (this.state == GhostState.CHASE)
            return this.target;
        else {
            return this.scatterTarget;
        }
    }

    public void setTarget(Coordinate target) {
        this.target=target;
    }

    public Coordinate getSpawn() {
        return this.spawn;
    }

    public Tile getTile() {
        return this.tile;
    }

    public String getName() {
        return this.name;
    }

    public GhostState getState() {
        return this.state;
    }

    public void setState(GhostState state) {
        this.state = state;
    }

    public Coordinate getNextPosition(PacManModel model) {

        boolean[][] correctPath = model.getBoard().solveMaze(this.position, this.target);

        // for (int r=0; r< correctPath.length; r++) {
        //     for (int c=0; c < correctPath[r].length; c++) {
        //         if (correctPath[r][c]) {
        //             System.out.println("Row="+r+" Col="+c);
        //         }
        //     }
        // }
        // System.out.println("Path printed");

        int currentRow = this.position.row;
        int currentCol = this.position.col;

        // Checks which direction is the correct path:

        // Up:
        int deltaRow=-1;
        int deltaCol=0;
        if (model.moveIsLegal(deltaRow, deltaCol, this.getPosition()) && !correctPath[currentRow+deltaRow][currentCol+deltaCol]) {
            return new Coordinate(currentRow+deltaRow, currentCol+deltaCol);
        }

        // Down:
        deltaRow=1;
        if (model.moveIsLegal(deltaRow, deltaCol, this.getPosition()) && !correctPath[currentRow+deltaRow][currentCol+deltaCol]) {
            return new Coordinate(currentRow+deltaRow, currentCol+deltaCol);
        }

        // Left:
        deltaRow = 0;
        deltaCol = -1;
        if (model.moveIsLegal(deltaRow, deltaCol, this.getPosition()) && !correctPath[currentRow+deltaRow][currentCol+deltaCol]) {
            return new Coordinate(currentRow+deltaRow, currentCol+deltaCol);
        }

        // Right:
        deltaCol = 1;
        if (model.moveIsLegal(deltaRow, deltaCol, this.getPosition()) && !correctPath[currentRow+deltaRow][currentCol+deltaCol]) {
            return new Coordinate(currentRow+deltaRow, currentCol+deltaCol);
        }

        else {return this.position;}
    }

    public void updateGhostTarget(PacManModel model) {
        if (this.state==GhostState.SCATTER) {
            this.target = this.scatterTarget;
        }

        else {
        
            if (this.name.equals("Blinky")) {
                this.target = model.getPacMan().getPosition();
                //System.out.println("Blinky target updated to"+this.target);
            }
            else if (this.name.equals("Inky")) {
                this.target = model.getPacMan().getPosition();
                //System.out.println("Inky target updated to"+this.target);
            }
            else if (this.name.equals("Pinky")) {
                Coordinate PacManPosition = model.getPacMan().getPosition();
                this.target = new Coordinate(PacManPosition.row, PacManPosition.col+2);
                //System.out.println("Pinky target updated to"+this.target);
            }
            else if (this.name.equals("Clyde")) {
                if (this.position == null) {
                    //System.out.println("Pos " + this.position + " for " + this.name + " was null!");
                } 
                else if (this.position.gridDistanceTo(model.getPacMan().getPosition()) < 8) {
                    this.target = this.scatterTarget;
                    //System.out.println("Clyde target updated to"+this.target);

                }
                else {
                    this.target = model.getPacMan().getPosition();
                    //System.out.println("Clyde target updated to"+this.target);
                }
            }
        
            else {this.target = this.initialTarget;}
        }
    }

    public void setPosition(Coordinate cords) {
        this.position = cords;
    }

    // Making the 4 ghosts - initial position will be spawn position:
    // Setting inital target as middle of the labyrinth:
    static Coordinate initialTarget = new Coordinate(7, 9);

    // Blinky:
    static Coordinate BlinkySpawn = new Coordinate(7, 9);
    static Coordinate BlinkyScatterTarget = new Coordinate(1, 17);
    static Tile BlinkyTile = new Tile(Color.RED, 'B');
    static Ghost Blinky = new Ghost("Blinky", BlinkySpawn, initialTarget, BlinkySpawn, BlinkyScatterTarget, BlinkyTile);
    
    // Inky:
    static Coordinate InkySpawn = new Coordinate(9, 8);
    static Coordinate InkyScatterTarget = new Coordinate(17, 17);
    static Tile InkyTile = new Tile(Color.CYAN, 'I');
    static Ghost Inky = new Ghost("Inky" ,InkySpawn, initialTarget, InkySpawn, InkyScatterTarget, InkyTile);

    // Pinky:
    static Coordinate PinkySpawn = new Coordinate(9, 9);
    static Coordinate PinkyScatterTarget = new Coordinate(1, 1);
    static Tile PinkyTile = new Tile(Color.PINK, 'P');
    static Ghost Pinky = new Ghost("Pinky", PinkySpawn, initialTarget, PinkySpawn, PinkyScatterTarget, PinkyTile);

    // Clyde:
    static Coordinate ClydeSpawn = new Coordinate(9, 10);
    static Coordinate ClydeScatterTarget = new Coordinate(17, 1);
    static Tile ClydeTile = new Tile(Color.ORANGE, 'C');
    static Ghost Clyde = new Ghost("Clyde" ,ClydeSpawn, initialTarget, ClydeSpawn, ClydeScatterTarget, ClydeTile);

    public static Ghost[] PACMAN_GHOSTS = new Ghost[] {Blinky, Inky, Pinky, Clyde};

    public static Ghost[] getPacManGhosts() {
        return PACMAN_GHOSTS;
    }

    
    /**
     * Resets the ghost to spawn values
     */
    public void resetGhost() {
        if (this.name == "Blinky") {
            this.position = BlinkySpawn;
        }
        else if (this.name == "Inky") {
            this.position = InkySpawn;
        }
        else if (this.name == "Pinky") {
            this.position = PinkySpawn;
        }
        else {
            this.position = ClydeSpawn;
        }

        this.target=initialTarget;
        this.state=GhostState.CHASE;
    }

}
