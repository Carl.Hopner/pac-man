package inf101v22.pacman.model;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import inf101v22.grid.Coordinate;
import inf101v22.grid.CoordinateItem;
import inf101v22.pacman.controller.PacManControllable;
import inf101v22.pacman.model.Ghosts.Ghost;
import inf101v22.pacman.model.Ghosts.GhostState;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;
import inf101v22.pacman.model.PacManPlayer.PacManState;
import inf101v22.pacman.view.PacManViewable;

/**
 * This class represents everything that is viewable and controllable in a pac-man game: the board, pacman and the ghosts.
 * It also keeps tabs on values like scores, number of lives and wether the ghosts are afraid of pacman ore not.
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class PacManModel implements PacManViewable, PacManControllable {

    private PacManPlayer PacMan;
    private Ghost[] ghosts;
    private PacManBoard board;
    private GameScreen screen;
    private int score;
    private int highScore = 0;
    private int numLives;
    private int numMoves;
    private boolean scatterGhosts = false;
    private int scatterTimer;
    // Toggles the AI of the ghosts (smart ghosts does not work currently):
    private boolean smartGhosts = false;

    private int startingTimeStep = 500;
    
    public PacManModel() {
        this.board = new PacManBoard(Tile.COIN);
        this.PacMan = new PacManPlayer(new Coordinate(11, 9), PacManState.UP);
        this.ghosts = Ghost.getPacManGhosts();

        this.numLives=3;
        this.numMoves=0;

        this.screen = GameScreen.WELCOME;
    }


    @Override
    public int getRows() {
        return this.board.rows;
    }

    @Override
    public int getCols() {
        return this.board.cols;
    }

    @Override
    public Iterable<CoordinateItem<Tile>> tilesOnBoard() {
        return this.board;
    }


    @Override
    public GameScreen getGameScreen() {
        return this.screen;
    }

    public PacManBoard getBoard() {
        return this.board;
    }


    @Override
    public int getScore() {
        return this.score;
    }


    public void startGame() {
        this.screen = GameScreen.ACTIVE_GAME;
    }


    @Override
    public int getHighScore() {
        return this.highScore;
    }

    @Override
    public int getNumLives() {
        return this.numLives;
    }


    @Override
    public void setHighScore(int score) {
        this.highScore = score;
        
    }


    @Override
    public void startNewGame() {
        this.board = new PacManBoard(Tile.COIN);
        this.PacMan = new PacManPlayer(new Coordinate(11, 9), PacManState.UP);
        for (Ghost ghost : ghosts) {
            ghost.resetGhost();
        }
        if (this.score>this.highScore) {
            setHighScore(this.score);
        }
        this.score = 0;
        this.numLives=3;
        this.numMoves = 0;
        this.screen = GameScreen.ACTIVE_GAME;
    }


    @Override
    public PacManPlayer getPacMan() {
        return this.PacMan;
    }


    @Override
    public boolean movePacMan(int deltaRow, int deltaCol) {
        int currentRow = this.PacMan.getPosition().row;
        int currentCol = this.PacMan.getPosition().col;
        int nextRow = currentRow+deltaRow;
        int nextCol = currentCol+deltaCol;
        Coordinate nextCoordinate = new Coordinate(nextRow, nextCol);

        if (nextCoordinate.equals(new Coordinate(9, -1))) {
            nextCoordinate = new Coordinate(9, 18);
        }
        if (nextCoordinate.equals(new Coordinate(9, 19))) {
            nextCoordinate = new Coordinate(9, 0);
        }

        if (this.board.coordinateIsOnGrid(nextCoordinate) && this.board.get(nextCoordinate)!=Tile.WALL) {
            if (this.board.get(nextCoordinate) == Tile.COIN) {
                this.score = this.score + 10;
                this.board.set(nextCoordinate, null);
            }
            if (this.board.get(nextCoordinate) == Tile.BIG_COIN) {
                this.scatterTimer = 0;
                this.scatterGhosts = true;
                for (Ghost ghost : ghosts) {
                    ghost.setState(GhostState.SCATTER);
                }
                this.board.set(nextCoordinate, null);
            }
            this.PacMan.setPosition(nextCoordinate);
            return true;
        }
        return false;
    }


    @Override
    public void setGameScreen(GameScreen screen) {
        this.screen = screen;
    }


    @Override
    public void moveGhosts() {
        for (Ghost ghost : this.ghosts) {
            Coordinate nextPos = ghost.getNextPosition(this);
            ghost.setPosition(nextPos);
        }
        
    }


    @Override
    public Ghost[] getGhosts() {
        return this.ghosts;
    }

    /**
     * Checks if it is allowed to move in given direction. You are not allowed to move into a wall.
     * @param deltaRow
     * @param deltaCol
     * @param currentPosition
     * @return True if the move is allowed, false otherwise
     */
    public Boolean moveIsLegal(int deltaRow, int deltaCol, Coordinate currentPosition) {
        int currentRow = currentPosition.row;
        int currentCol = currentPosition.col;
        int nextRow = currentRow+deltaRow;
        int nextCol = currentCol+deltaCol;
        Coordinate nextCoordinate = new Coordinate(nextRow, nextCol);

        if (nextCoordinate.equals(new Coordinate(9, -1))) {
            nextCoordinate = new Coordinate(9, 18);
        }
        if (nextCoordinate.equals(new Coordinate(9, 19))) {
            nextCoordinate = new Coordinate(9, 0);
        }

        if (this.board.coordinateIsOnGrid(nextCoordinate) && this.board.get(nextCoordinate)!=Tile.WALL) {
            return true;
        }
        else {return false;}
    }


    @Override
    public void updateGhostTarget() {
        for (Ghost ghost : this.ghosts) {
            ghost.updateGhostTarget(this);
        }
        
    }


    @Override
    public void moveGhostsRandom() {
        int randomRow;
        int randomCol;
        for (Ghost ghost : this.ghosts) {
            int currentRow = ghost.getPosition().row;
            int currentCol = ghost.getPosition().col;

            // First checks if pacman is nearby:
            if (this.PacMan.getPosition().equals(new Coordinate(currentRow+1, currentCol))) {
                ghost.setPosition(new Coordinate(currentRow+1, currentCol));
            }
            else if (this.PacMan.getPosition().equals(new Coordinate(currentRow-1, currentCol))) {
                ghost.setPosition(new Coordinate(currentRow-1, currentCol));
            }
            else if (this.PacMan.getPosition().equals(new Coordinate(currentRow, currentCol+1))) {
                ghost.setPosition(new Coordinate(currentRow, currentCol+1));
            }
            else if (this.PacMan.getPosition().equals(new Coordinate(currentRow, currentCol-1))) {
                ghost.setPosition(new Coordinate(currentRow, currentCol-1));
            }
            
            else {
                List<Integer> deltas = Arrays.asList(-1, 0, 1);
                List<Integer> deltasNoZero = Arrays.asList(-1, 1);
                Random rand = new Random();
                randomRow = deltas.get(rand.nextInt(deltas.size()));
                if (randomRow==1 | randomRow==-1) {
                    randomCol = 0;
                }
                else {
                    randomCol = deltas.get(rand.nextInt(deltasNoZero.size()));
                }
                if (moveIsLegal(randomRow, randomCol, ghost.getPosition())) {
                    ghost.setPosition(new Coordinate(currentRow+randomRow, currentCol+randomCol));
                }
                else {
                    Coordinate ghostPosition = ghost.getPosition();
                    // Blinky is more likely to go up and right:
                    if (ghost.getName().equals("Blinky")) {
                        if (moveIsLegal(-1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row-1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, 1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 18))) {
                                ghost.setPosition(new Coordinate(9, 0));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col+1));}
                        }
                        else if (moveIsLegal(1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row+1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, -1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 0))) {
                                ghost.setPosition(new Coordinate(9, 18));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col-1));}
                        }
                    }
                    // Inky is more likely to go right and down:
                    else if (ghost.getName().equals("Inky")) {
                        if (moveIsLegal(0, 1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 18))) {
                                ghost.setPosition(new Coordinate(9, 0));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col+1));}
                        }
                        else if (moveIsLegal(1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row+1, ghostPosition.col));
                        }
                        else if (moveIsLegal(-1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row-1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, -1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 0))) {
                                ghost.setPosition(new Coordinate(9, 18));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col-1));}
                        }
                    }
                    // Pinky is more likely to go up and left:
                    else if (ghost.getName().equals("Pinky")) {
                        if (moveIsLegal(-1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row-1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, -1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 0))) {
                                ghost.setPosition(new Coordinate(9, 18));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col-1));}
                        }
                        else if (moveIsLegal(1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row+1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, 1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 18))) {
                                ghost.setPosition(new Coordinate(9, 0));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col+1));}
                        }
                    }
                    // Clyde is more likely to move left and down:
                    else if (ghost.getName().equals("Clyde")) {
                        if (moveIsLegal(0, -1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 0))) {
                                ghost.setPosition(new Coordinate(9, 18));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col-1));}
                        }
                        else if (moveIsLegal(1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row+1, ghostPosition.col));
                        }
                        else if (moveIsLegal(-1, 0, ghostPosition)) {
                            ghost.setPosition(new Coordinate(ghostPosition.row-1, ghostPosition.col));
                        }
                        else if (moveIsLegal(0, 1, ghostPosition)) {
                            if (ghostPosition.equals(new Coordinate(9, 18))) {
                                ghost.setPosition(new Coordinate(9, 0));
                            }
                            else {ghost.setPosition(new Coordinate(ghostPosition.row, ghostPosition.col+1));}
                        }
                    }
                }
            }
        }    
    }


    @Override
    public void checkState() {
        // Checks if pac-man have been eaten by or has eaten any ghosts.
        for (Ghost ghost : ghosts) {
            if (PacMan.getPosition().equals(ghost.getPosition())) {
                if (ghost.getState() == GhostState.SCATTER) {
                    this.score = this.score+100;
                    ghost.resetGhost();
                }
                else {
                    if (numLives==0) {
                        this.screen = GameScreen.GAME_OVER;
                    }
                    else {
                        this.numLives--;
                        this.PacMan = new PacManPlayer(new Coordinate(11, 9), PacManState.UP);
                        for (Ghost ghost_ : ghosts) {
                            ghost_.resetGhost();
                        }
                        break;
                    }
                }
            }
        }
        // Checks if there is any coins left on board. Resets the board if empty.
        boolean hasCoin = false;
        for (CoordinateItem<Tile> tile : this.board) {
            if (tile.item == Tile.COIN) {
                hasCoin = true;
            }
        }
        if (!hasCoin) {
            this.PacMan = new PacManPlayer(new Coordinate(11, 9), PacManState.UP);
            this.board = new PacManBoard(Tile.COIN);
            for (Ghost ghost_ : ghosts) {
                ghost_.resetGhost();
            }
        }
        
    }


    @Override
    public void clockTick() {
        
        // Moving Pac-Man:
        if (this.PacMan.getState() == PacManState.UP) {
            movePacMan(-1, 0);
        }
        else if (this.PacMan.getState() == PacManState.DOWN) {
            movePacMan(1, 0);
        }
        else if (this.PacMan.getState() == PacManState.RIGHT) {
            movePacMan(0, 1);
        }
        else if (this.PacMan.getState() == PacManState.LEFT) {
            movePacMan(0, -1);
        }
        this.numMoves++;

        if (scatterGhosts) {
            if (scatterTimer == 30) {
                scatterGhosts = false;
                for (Ghost ghost : ghosts) {
                    ghost.setState(GhostState.CHASE);
                }
            }
            else {
                scatterTimer++;
            }
        }
        checkState();

        // Updating and moving Ghosts:
        updateGhostTarget();
        if (this.smartGhosts) {
            moveGhosts();
        }
        else {

            moveGhostsRandom();
        }    

        // Checks the board:
        checkState();
        
    }


    @Override
    public double getTimeStep() {
        double timeStep = this.startingTimeStep*java.lang.Math.pow(0.98, this.numMoves/10);
        return timeStep;
    }


}
