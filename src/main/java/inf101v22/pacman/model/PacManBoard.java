package inf101v22.pacman.model;

import java.util.ArrayList;

import inf101v22.grid.Coordinate;
import inf101v22.grid.Grid;

/**
 * This class represents the Pac Man maze. A grid of tiles.
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class PacManBoard extends Grid<Tile> {

    private boolean[][] maze = {
        {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
        {false, true, true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, true, false},
        {false, true, false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, true, false},
        {false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false},
        {false, true, false, false, true, false, true, false, false, false, false, false, true, false, true, false, false, true, false},
        {false, true, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, false},
        {false, false, false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, false, false},
        {true, true, true, false, true, false, true, true, true, true, true, true, true, false, true, false, true, true, true},
        {false, false, false, false, true, false, true, false, false, true, false, false, true, false, true, false, false, false, false},
        {true, true, true, true, true, true, true, false, true, true, true, false, true, true, true, true, true, true, true},
        {false, false, false, false, true, false, true, false, false, false, false, false, true, false, true, false, false, false, false},
        {true, true, true, false, true, false, true, true, true, true, true, true, true, false, true, false, true, true, true},
        {false, false, false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, false, false},
        {false, true, true, true, true, false, true, true, true, false, true, true, true, false, true, true, true, true, false},
        {false, true, false, false, true, false, true, false, false, false, false, false, true, false, true, false, false, true, false},
        {false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false},
        {false, true, false, false, true, false, false, false, true, false, true, false, false, false, true, false, false, true, false},
        {false, true, true, true, true, true, true, true, true, false, true, true, true, true, true, true, true, true, false},
        {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}
    };

    private Coordinate[] nullValues = new Coordinate[] {
        new Coordinate(7, 0),
        new Coordinate(7, 1),
        new Coordinate(7, 2),
        new Coordinate(7, 18),
        new Coordinate(7, 17),
        new Coordinate(7, 16),
        new Coordinate(11, 0),
        new Coordinate(11, 1),
        new Coordinate(11, 2),
        new Coordinate(11, 18),
        new Coordinate(11, 17),
        new Coordinate(11, 16),
        new Coordinate(9, 9),
        new Coordinate(9, 8),
        new Coordinate(9, 10),
        new Coordinate(8, 9)
    };

    private Coordinate[] bigCoins = new Coordinate[] {
        new Coordinate(1, 1),
        new Coordinate(1, 17),
        new Coordinate(17, 1),
        new Coordinate(17, 17)
    };

    public PacManBoard(Tile value) {
        super(19, 19, value);
        for (int r = 0; r<19; r++) {
            for (int c=0; c<19; c++) {
                Coordinate coordinate = new Coordinate(r, c);
                if (!this.maze[r][c]) {
                    super.set(coordinate, Tile.WALL);
                }
                else {
                    for (Coordinate nullCoordinate : this.nullValues) {
                        if (coordinate.equals(nullCoordinate)) {
                            super.set(coordinate, null);
                        }
                    for (Coordinate bigCoinCoordinate : this.bigCoins) {
                        if (coordinate.equals(bigCoinCoordinate)) {
                            super.set(coordinate, Tile.BIG_COIN);
                        }
                    }
                    }
                }
            }
        }
    }

    public PacManBoard() {
        super(19, 19, null);
        for (int r = 0; r<19; r++) {
            for (int c=0; c<19; c++) {
                if (!this.maze[r][c]) {
                    super.set(new Coordinate(r, c), Tile.WALL);
                }
            }
        }
    }

    public boolean[][] getMaze() {
        return this.maze;
    }

    /**
     * @return Char[][] with Tile characters or '-' for null.
     */
    public char[][] toCharArray2d() {
        char[][] charArray = new char[this.rows][ this.cols];
        for (int r=0; r<this.rows; r++) {
            for (int c=0; c<this.cols; c++) {
                if (this.get(new Coordinate(r, c))==null) {
                    charArray[r][c] = '-';
                }
                else if (this.get(new Coordinate(r, c)) instanceof Tile) {
                    charArray[r][c] = this.get(new Coordinate(r, c)).c;
                }
            }
        }
        return charArray;
    }

    /**
     * Takes a char[][] and turns it into a string with '\n' between.
     * @param charArray char[][]
     * @return String
     */
    public static String toCharArray2dToString(char[][] charArray) {
        ArrayList<String> stringList1d = new ArrayList<String>();
        for (char[] r : charArray) {
            stringList1d.add(String.valueOf(r)+'\n');
        }
        return String.join("", stringList1d);
    }

    /**
     * Checks if the value null exists on a specific row
     * @param row
     * @return true if null exists on row, false otherwise
     */
    public boolean rowIsFull(int row) {
        for (int col = 0; col < getCols(); col++) {
            if (get(new Coordinate(row, col)) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Sets entire row to given value
     * @param row
     * @param value
     */
    public void setRow(int row, Tile value) {
        for (int col = 0; col < getCols(); col++) {
            set(new Coordinate(row, col), value);
        }
    }

    /**
     * Sets all tiles on board to null.
     * @param numRows
     */
    public void emptyBoard(int numRows) {
        for (int r=0; r<numRows; r++) {
            setRow(r, null);
        }
    }

    // The code below is an edited version of a code copied from the 
    // wikipedia page "Maze-solving algorithm". It was downloaded on 13/4-22
    // Link: https://en.wikipedia.org/wiki/Maze-solving_algorithm

    boolean[][] wasHere = new boolean[this.cols][this.rows];
    boolean[][] correctPath = new boolean[this.cols][this.rows]; // The solution to the maze

    public boolean[][] solveMaze(Coordinate start, Coordinate end) {
        
        int startX = start.col;
        int startY = start.row;
        int endX = end.col;
        int endY = end.row;

        for (int row = 0; row < maze.length; row++)  
            // Sets boolean Arrays to default values
            for (int col = 0; col < maze[row].length; col++){
                wasHere[row][col] = false;
                correctPath[row][col] = false;
            }
        boolean b = recursiveSolve(startX, startY, endX, endY);
        // Will leave you with a boolean array (correctPath) 
        // with the path indicated by true values.
        // If b is false, there is no solution to the maze

       return correctPath;
    }
    public boolean recursiveSolve(int x, int y, int endX, int endY) {
        boolean[][] invertedmaze = new boolean[this.rows][this.cols];
        for (int r=0; r<this.rows; r++) {
            for (int c=0; c<this.cols; c++) {
                if (this.maze[r][c]) {
                    invertedmaze[r][c] = false;
                }
                else {invertedmaze[r][c]=true;}
            }
        }

        if (x == endX && y == endY) return true; // If you reached the end
        if (invertedmaze[x][y] || wasHere[x][y]) return false;
        // If you are on a wall or already were here
        wasHere[x][y] = true;
        if (x != 0) // Checks if not on left edge
            if (recursiveSolve(x-1, y, endX, endY)) { // Recalls method one to the left
                correctPath[x][y] = true; // Sets that path value to true;
                return true;
            }
        if (x != this.cols - 1) // Checks if not on right edge
            if (recursiveSolve(x+1, y, endX, endY)) { // Recalls method one to the right
                correctPath[x][y] = true;
                return true;
            }
        if (y != 0)  // Checks if not on top edge
            if (recursiveSolve(x, y-1, endX, endY)) { // Recalls method one up
                correctPath[x][y] = true;
                return true;
            }
        if (y != this.rows - 1) // Checks if not on bottom edge
            if (recursiveSolve(x, y+1, endX, endY)) { // Recalls method one down
                correctPath[x][y] = true;
                return true;
            }
        return false;
    }
    
}
