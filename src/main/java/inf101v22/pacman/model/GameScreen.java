package inf101v22.pacman.model;

/**
 * Represents different states the game is in
 */
public enum GameScreen {

    WELCOME,
    ACTIVE_GAME,
    GAME_OVER,
    PAUSE;
    
}
