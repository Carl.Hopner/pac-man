package inf101v22.pacman.model;

import java.awt.Color;

/**
 * This class represents one tile on a board.
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class Tile {

    public final java.awt.Color color;
    public final char c;

    public Tile(java.awt.Color color, char c) {
        
        this.color = color;
        this.c = c;

    }

    public final static Tile WALL = new Tile(Color.BLUE, 'w');
    public final static Tile COIN = new Tile(Color.WHITE, 'c');
    public final static Tile BIG_COIN = new Tile(Color.WHITE, 'C');
    
}
