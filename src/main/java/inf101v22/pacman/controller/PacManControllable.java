package inf101v22.pacman.controller;

import inf101v22.pacman.model.GameScreen;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;

public interface PacManControllable {

    /**
     * @return active game screen
     */
    GameScreen getGameScreen();

    /**
     * Sets game screen to active.
     */
    void startGame();

    /**
     * Starts a new game.
     */
    void startNewGame();

    /**
     * @param screen that should be activated.
     */
    void setGameScreen(GameScreen screen);

    /**
     * @return Pac-Man
     */
    PacManPlayer getPacMan();

    /**
     * Moves Pac-Man
     * @param deltaRow
     * @param deltaCol
     * @return True if move was done
     */
    boolean movePacMan(int deltaRow, int deltaCol);

    /**
     * Moves all ghosts on board.
     */
    void moveGhosts();

    /**
     * Moves all ghosts in a random direction
     */
    void moveGhostsRandom();

    /**
     * Updates the target of all Ghosts
     */
    void updateGhostTarget();

    /**
     * Checks if pac-man should die or if board should be reset.
     */
    void checkState();

    /**
     * Performs one time step
     */
    void clockTick();

    /**
     * Gets current time step.
     */
    double getTimeStep();

}
