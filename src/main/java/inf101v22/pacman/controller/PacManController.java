package inf101v22.pacman.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Timer;

import inf101v22.pacman.model.GameScreen;
import inf101v22.pacman.model.PacManPlayer.PacManState;
import inf101v22.pacman.view.PacManView;

public class PacManController implements java.awt.event.KeyListener, java.awt.event.ActionListener {

    PacManControllable model;
    PacManView view;
    javax.swing.Timer timer;

    public PacManController(PacManControllable model, PacManView view) {
        this.model = model;
        this.view = view;
        this.timer = new javax.swing.Timer((int) model.getTimeStep(), this);
        view.addKeyListener(this);

        timer.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (model.getGameScreen()==GameScreen.ACTIVE_GAME) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                // Left arrow was pressed
                this.model.getPacMan().setState(PacManState.LEFT);
            }
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                // Right arrow was pressed
                this.model.getPacMan().setState(PacManState.RIGHT);
            }
            else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                // Down arrow was pressed
                this.model.getPacMan().setState(PacManState.DOWN);
            }
            else if (e.getKeyCode() == KeyEvent.VK_UP) {
                // Up arrow was pressed
                this.model.getPacMan().setState(PacManState.UP);
            }
            else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                // Esc was pressed
                this.model.setGameScreen(GameScreen.PAUSE);
            }
            else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                this.model.setGameScreen(GameScreen.GAME_OVER);
            }
        }

        else if (model.getGameScreen()==GameScreen.WELCOME) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                this.model.startGame();
            }
        }
        
        else if (model.getGameScreen()==GameScreen.GAME_OVER) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                this.model.startNewGame();
            }
        }
        else if (model.getGameScreen()==GameScreen.PAUSE) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                this.model.setGameScreen(GameScreen.ACTIVE_GAME);
            }
        }


        this.view.repaint();
        
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (model.getGameScreen() == GameScreen.ACTIVE_GAME) {
            model.clockTick();
            this.view.repaint();
            modelTime();
        }
        
    }

    public void modelTime() {
        timer.setDelay((int) model.getTimeStep());
        timer.setInitialDelay((int) model.getTimeStep());
    }
    
}
