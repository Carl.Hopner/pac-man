package inf101v22.pacman.view;

import inf101v22.grid.CoordinateItem;
import inf101v22.pacman.model.GameScreen;
import inf101v22.pacman.model.Tile;
import inf101v22.pacman.model.Ghosts.Ghost;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;

public interface PacManViewable {

    /**
     * @return number of rows in grid.
     */
    int getRows();

    /**
     * @return number of columns in grid.
     */
    int getCols();
    
    /** 
     * @return an iterable. 
     */
    Iterable<CoordinateItem<Tile>> tilesOnBoard();

    /**
     * @return active game screen
     */
    GameScreen getGameScreen();

    /**
     * @return Current score
     */
    int getScore();

    /**
     * @return Current high score
     */ 
    int getHighScore();

    /**
     * @return Number of lives left.
     */
    int getNumLives();

    /**
     * Sets new high score
     * @param score
     */
    void setHighScore(int score);

    /**
     * @return PacMan with position.
     */
    PacManPlayer getPacMan();

    /**
     * @return Iterable of all ghosts on board.
     */
    Ghost[] getGhosts();

}
