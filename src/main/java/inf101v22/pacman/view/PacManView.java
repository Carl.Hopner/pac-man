package inf101v22.pacman.view;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.imageio.ImageIO;

import inf101v22.grid.CoordinateItem;
import inf101v22.pacman.model.GameScreen;
import inf101v22.pacman.model.Tile;
import inf101v22.pacman.model.Ghosts.Ghost;
import inf101v22.pacman.model.Ghosts.GhostState;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;
import inf101v22.pacman.model.PacManPlayer.PacManState;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

/**
 * This class draws everything we see on the Pac-Man board.
 * 
 * @author Carl Richard Alexander Kvernberg Höpner - tir016@uib.no
 */
public class PacManView extends JComponent{

    PacManViewable model;

    Image logo;
    Image big_coin;
    
    Image down;
    Image up;
    Image right;
    Image left;

    Image Blinky;
    Image Inky;
    Image Pinky;
    Image Clyde;
    Image scatter;

    { // Here, all the images are loaded
        try {
            logo = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/logo.png"))).getImage();
            big_coin = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/big_coin.png"))).getImage();


            down = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/down.gif"))).getImage();
            up = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/up.gif"))).getImage();
            right = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/right.gif"))).getImage();
            left = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/left.gif"))).getImage();

            Blinky = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/blinky.png"))).getImage();
            Inky = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/inky.png"))).getImage();
            Pinky = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/pinky.png"))).getImage();
            Clyde = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/clyde.png"))).getImage();
            scatter = new ImageIcon(ImageIO.read(PacManView.class.getClassLoader().getResource("./images/scatter.jpeg"))).getImage();
            
        } catch (Exception e) {
            System.out.println("Pictures not found!");
        }
        }


    public PacManView(PacManViewable model) {
        this.model=model;
    }

    {this.setFocusable(true);}

    @Override
    public void paint(Graphics board) {

        super.paint(board);

        int padding = 0; //Can be added if wanted

        if (model.getGameScreen()==GameScreen.WELCOME) {
            board.setColor(Color.black);
            board.fillRect(0, 0, this.getWidth(), this.getHeight());
            board.drawImage(logo, 75, 0, 600, 600, this);
            board.setColor(Color.YELLOW);
            Font myFont = new Font("SansSerif", Font.BOLD, 42);
            board.setFont(myFont);
            GraphicHelperMethods.drawCenteredString(board, "WELCOME", 0, 50, this.getWidth(), this.getHeight());
            board.setFont(new Font("SansSerif", Font.BOLD, 16));
            GraphicHelperMethods.drawCenteredString(board, "Press spacebar to play", 0, 100, this.getWidth(), this.getHeight());
        }
        
        else if (model.getGameScreen() == GameScreen.GAME_OVER) {
            board.setColor(Color.BLACK);
            board.fillRect(0, 0, this.getWidth(), this.getHeight());
            board.setColor(Color.GREEN);
            Font myFont = new Font("SansSerif", Font.BOLD, 16);
            board.setFont(myFont);
            GraphicHelperMethods.drawCenteredString(board, "GAME OVER", 0, 0, this.getWidth(), this.getHeight());
            String scoreline = String.format("Score: %d", model.getScore());
            GraphicHelperMethods.drawCenteredString(board, scoreline, 0, 125, this.getWidth(), this.getHeight());
            String highScoreline = String.format("High Score: %d", model.getHighScore());
            GraphicHelperMethods.drawCenteredString(board, highScoreline, 0, 100, this.getWidth(), this.getHeight());
            GraphicHelperMethods.drawCenteredString(board, "Press enter to play again", 0, 200, this.getWidth(), this.getHeight());
        }

        else {
            this.drawMaze(board, 0,0,this.getWidth(),this.getHeight(), padding);
            this.drawPacMan(board, this.getWidth(), this.getHeight(), padding);
            this.drawGhosts(board, this.getWidth(), this.getHeight(), padding);
            this.drawLivesandScore(board);
            if (model.getGameScreen() == GameScreen.PAUSE) {
                board.setColor(Color.GREEN);
                Font myFont = new Font("SansSerif", Font.BOLD, 16);
                board.setFont(myFont);
                GraphicHelperMethods.drawCenteredString(board, "PAUSE", 0, 0, this.getWidth(), this.getHeight());
                String scoreline = String.format("Score: %d", model.getScore());
                GraphicHelperMethods.drawCenteredString(board, scoreline, 0, 125, this.getWidth(), this.getHeight());
                String highScoreline = String.format("High Score: %d", model.getHighScore());
                GraphicHelperMethods.drawCenteredString(board, highScoreline, 0, 100, this.getWidth(), this.getHeight());
                GraphicHelperMethods.drawCenteredString(board, "Press enter to continue", 0, 200, this.getWidth(), this.getHeight());
            }
        }
    }

    /**
     * Draws the Pacman board.
     * @param canvas where the board should be drawn
     * @param x coordinate of top left corner
     * @param y coordinate of top left corner
     * @param width
     * @param height
     * @param padding width
     */
    private void drawMaze(Graphics canvas, int x, int y, int width, int height, int padding) {
        canvas.setColor(Color.WHITE);
        canvas.fillRect(x, y, padding, height);
        canvas.fillRect(x, y-padding, width, padding);
        drawBoard(canvas, x+padding, y+padding, width, height, padding);
    }

    @Override
    public Dimension getPreferredSize() {
        int width = 750;
        int height = 750;
        return new Dimension(width, height);
    }

    /**
     * Draws å single tile with padding on the bottom and right side.
     * @param canvas where the tile should be drawn
     * @param x coordinate of top left corner
     * @param y coordinate of top left corner
     * @param width of tile with padding
     * @param height of tile with padding
     * @param padding width
     * @param color of tile
     */
    private void drawTileWithRightBottomPadding(Graphics canvas, int x, int y, int width, int height, int padding, java.awt.Color color) {
        canvas.setColor(color);
        canvas.fillRect(x, y, width-padding, height-padding);
        //canvas.setColor(Color.WHITE);
        //canvas.fillRect(x+width-padding, y, padding, height);
        //canvas.fillRect(x, y+height-padding, width, padding);
    }
    
    /**
     * Draws an entire board of tiles with padding on bottom and right side.
     * @param canvas where the board should be drwan
     * @param x coordinate of top left corner
     * @param y coordinate of top left corner
     * @param boardWidth
     * @param boardHeight
     * @param padding width
     */
    private void drawBoard(Graphics canvas, int x, int y, int boardWidth, int boardHeight, int padding) {
        canvas.setColor(Color.BLACK);
        canvas.fillRect(x, y, boardWidth, boardHeight);
        for (CoordinateItem<Tile> coordinateItem : this.model.tilesOnBoard()) {
            int row = coordinateItem.coordinate.row;
            int col = coordinateItem.coordinate.col;
            Tile tile = coordinateItem.item;
            Color color = Color.BLACK;
            if (tile != null) {
                color = tile.color;
            }
            
            int tileX = x + col * boardWidth / this.model.getCols();
            int tileY = y + row * boardHeight / this.model.getRows();
            int nextTileX = x + (col + 1) * boardWidth / this.model.getCols();
            int nextTileY = y + (row + 1) * boardHeight / this.model.getRows();
            int tileWidth = nextTileX - tileX;
            int tileHeight = nextTileY - tileY;

            if (tile == Tile.COIN) {
                this.drawTileWithRightBottomPadding(canvas, tileX+2*tileWidth/5, tileY+2*tileHeight/5, tileWidth/5, tileHeight/5, padding, color);
            }
            else if (tile == Tile.BIG_COIN) {
                canvas.drawImage(big_coin, tileX, tileY, tileWidth, tileHeight, this);
            }
            
            else {this.drawTileWithRightBottomPadding(canvas, tileX, tileY, tileWidth, tileHeight, padding, color);}
        }
    }

    /**
     * Draws pac-man in the correct position
     * @param canvas
     * @param boardWidth
     * @param boardHeight
     * @param padding
     */
    private void drawPacMan(Graphics canvas, int boardWidth, int boardHeight, int padding) {
        PacManPlayer PacMan = this.model.getPacMan();
        Color color = PacMan.getTile().color;

        int row = PacMan.getPosition().row;
        int col = PacMan.getPosition().col;

        int tileX = col * boardWidth / this.model.getCols();
        int tileY = row * boardHeight / this.model.getRows();
        int nextTileX = (col + 1) * boardWidth / this.model.getCols();
        int nextTileY = (row + 1) * boardHeight / this.model.getRows();
        int tileWidth = nextTileX - tileX;
        int tileHeight = nextTileY - tileY;

        // this.drawTileWithRightBottomPadding(canvas, tileX, tileY, tileWidth, tileHeight, padding, color);
        if (PacMan.getState()==PacManState.DOWN) {
            canvas.drawImage(down, tileX, tileY, tileWidth, tileHeight, this);
        }
        if (PacMan.getState()==PacManState.UP) {
            canvas.drawImage(up, tileX, tileY, tileWidth, tileHeight, this);
        }
        if (PacMan.getState()==PacManState.LEFT) {
            canvas.drawImage(left, tileX, tileY, tileWidth, tileHeight, this);
        }
        if (PacMan.getState()==PacManState.RIGHT) {
            canvas.drawImage(right, tileX, tileY, tileWidth, tileHeight, this);
        }
    }

    /**
     * Draws all the ghost in the correct position
     * @param canvas
     * @param boardWidth
     * @param boardHeight
     * @param padding
     */
    private void drawGhosts(Graphics canvas, int boardWidth, int boardHeight, int padding) {
        for (Ghost ghost : this.model.getGhosts()) {
            Color color = ghost.getTile().color;

            int row = ghost.getPosition().row;
            int col = ghost.getPosition().col;

            int tileX = col * boardWidth / this.model.getCols();
            int tileY = row * boardHeight / this.model.getRows();
            int nextTileX = (col + 1) * boardWidth / this.model.getCols();
            int nextTileY = (row + 1) * boardHeight / this.model.getRows();
            int tileWidth = nextTileX - tileX;
            int tileHeight = nextTileY - tileY;

            
            if (ghost.getState()==GhostState.SCATTER) {
                canvas.drawImage(scatter, tileX, tileY, tileWidth, tileHeight, this);
            }
            else if (ghost.getName().equals("Blinky")) {
                canvas.drawImage(Blinky, tileX, tileY, tileWidth, tileHeight, this);
            }
            else if (ghost.getName().equals("Inky")) {
                canvas.drawImage(Inky, tileX, tileY, tileWidth, tileHeight, this);
            }
            else if (ghost.getName().equals("Pinky")) {
                canvas.drawImage(Pinky, tileX, tileY, tileWidth, tileHeight, this);
            }
            else if (ghost.getName().equals("Clyde")) {
                canvas.drawImage(Clyde, tileX, tileY, tileWidth, tileHeight, this);
            }

            else {this.drawTileWithRightBottomPadding(canvas, tileX, tileY, tileWidth, tileHeight, padding, color);}

        }
    }

    /**
     * Draws the small line of text in the bottom left corner showing score and lives.
     * @param canvas
     */
    private void drawLivesandScore(Graphics canvas) {
        String statusString = "Score: "+this.model.getScore()+"   Lives: "+this.model.getNumLives();
        canvas.setColor(Color.GREEN);
        canvas.drawString(statusString, 5, this.getHeight()-5);
    }

}
