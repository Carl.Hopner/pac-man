package inf101v22.pacman.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

import inf101v22.grid.Coordinate;
import inf101v22.pacman.model.Ghosts.GhostState;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;
import inf101v22.pacman.model.PacManPlayer.PacManState;

public class PacManModelTest {

    @Test
    void illegalPacManMoveTest() {
        PacManModel model = new PacManModel();
        PacManPlayer pacman = model.getPacMan();
        pacman.setPosition(new Coordinate(1, 1));
        assertFalse(model.movePacMan(-1, 0));
    }

    @Test
    void portalTest() {
        PacManModel model = new PacManModel();
        PacManPlayer pacman = model.getPacMan();
        pacman.setPosition(new Coordinate(9, 18));
        model.movePacMan(0, 1);
        Coordinate actual = pacman.getPosition();
        Coordinate expected = new Coordinate(9, 0);
        assertEquals(expected, actual);
    }

    @Test
    void bigCoinTest() {
        PacManModel model = new PacManModel();
        model.getPacMan().setPosition(new Coordinate(9, 1));
        model.getBoard().set(new Coordinate(9, 2), Tile.BIG_COIN);
        model.movePacMan(0, 1);
        assertEquals(GhostState.SCATTER, model.getGhosts()[0].getState());
    }

    @Test
    void clockTickTest() {
        PacManModel model = new PacManModel();
        PacManPlayer pacman = model.getPacMan();
        pacman.setPosition(new Coordinate(1, 1));
        pacman.setState(PacManState.RIGHT);
        model.clockTick();
        assertEquals(new Coordinate(1, 2), pacman.getPosition());
        
        model.clockTick();
        assertEquals(null, model.getBoard().get(new Coordinate(1, 2)));

    }
    
}
