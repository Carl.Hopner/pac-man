package inf101v22.pacman.model.Ghosts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

import inf101v22.grid.Coordinate;
import inf101v22.pacman.model.PacManBoard;
import inf101v22.pacman.model.PacManModel;
import inf101v22.pacman.model.PacManPlayer.PacManPlayer;
import inf101v22.pacman.model.PacManPlayer.PacManState;

public class GhostTest {

    @Test
    void getNextPositionTest() {
        PacManModel model = new PacManModel();
        model.getPacMan().setPosition(new Coordinate(7, 11));
        Ghost Blinky = model.getGhosts()[0];
        Blinky.updateGhostTarget(model);
        Coordinate actual = Blinky.getNextPosition(model);
        Coordinate expected = new Coordinate(7, 10);
        assertEquals(expected, actual);
    }

    @Test
    void resetGhostTest() {
        Ghost Blinky = Ghost.Blinky;
        Blinky.setPosition(new Coordinate(1, 1));
        Blinky.resetGhost();
        Coordinate actual = Blinky.getPosition();
        Coordinate expected = new Coordinate(7, 9);
        assertEquals(expected, actual);
    }
    
}
