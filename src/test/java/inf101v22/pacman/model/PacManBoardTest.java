package inf101v22.pacman.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

public class PacManBoardTest {

    @Test  
    void PacManMazeTest() {
        char[][] actual = new PacManBoard().toCharArray2d();
        String actualString = PacManBoard.toCharArray2dToString(actual);
        String expected = 
        "wwwwwwwwwwwwwwwwwww\n"+
        "w--------w--------w\n"+
        "w-ww-www-w-www-ww-w\n"+
        "w-----------------w\n"+
        "w-ww-w-wwwww-w-ww-w\n"+
        "w----w---w---w----w\n"+
        "wwww-www-w-www-wwww\n"+
        "---w-w-------w-w---\n"+
        "wwww-w-ww-ww-w-wwww\n"+
        "-------w---w-------\n"+
        "wwww-w-wwwww-w-wwww\n"+
        "---w-w-------w-w---\n"+
        "wwww-www-w-www-wwww\n"+
        "w----w---w---w----w\n"+
        "w-ww-w-wwwww-w-ww-w\n"+
        "w-----------------w\n"+
        "w-ww-www-w-www-ww-w\n"+
        "w--------w--------w\n"+
        "wwwwwwwwwwwwwwwwwww\n";

        assertEquals(expected, actualString);
    }
    
}
